<?php
if (!function_exists("secondsToShortTime")) {
    function secondsToShortTime($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds - ($hours * 60 * 60)) / 60);
        $seconds = $seconds - $hours * 3600 - $minutes * 60;
        return sprintf("%d:%d:%d",
            $hours,
            $minutes,
            $seconds
        );
    }
}
