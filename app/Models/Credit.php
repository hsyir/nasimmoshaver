<?php

namespace App\Models;

use App\User;
use Baloot\EloquentHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Credit extends Model
{
    use EloquentHelper;

    public function getUniqueUrlAttribute()
    {
        return route("front.credits.show", $this->unique_id);
    }


    public static function createNew($amount, $phone_number = null, $creditGroupId = null)
    {
        $credit = new Credit;
        $credit->identifier = self::createRandomPin();
        $credit->unique_id = self::creditUniqueId();
        $credit->phone_number = $phone_number;
        $credit->credit_group_id = $creditGroupId;
        $credit->creator_id = Auth::user() ? Auth::user()->id : null;
        $credit->save();

        $credit->chargeCredit($amount);

        return $credit;
    }

    public function changeUniqueId()
    {
        $this->unique_id = self::creditUniqueId();
        $this->save();
        return $this;
    }

    public function creator()
    {
        return $this->belongsTo(User::class, "creator_id")->withDefault([
            "name" => ""
        ]);
    }

    public function creditGroup()
    {
        return $this->belongsTo(CreditGroup::class);
    }

    public function chargeCredit($amount)
    {
        $this->charges()->create([
            'amount' => $amount,
            'creator_id' => Auth::user() ? Auth::user()->id : null,
        ]);
    }

    public function charges()
    {
        return $this->hasMany(CreditCharges::class);
    }

    public function calls()
    {
        return $this->hasMany(Call::class);
    }


    public function getTotalCreditAttribute()
    {
        return $this->charges->sum("amount");
    }

    public function getTotalUsageAttribute()
    {
        return $this->calls->sum("total_amount");
    }

    public function getRemainCreditAttribute()
    {
        return $this->total_credit - $this->total_usage;
    }


    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    public function scopeOrlike($query, $field, $value)
    {
        return $query->orWhere($field, 'LIKE', "%$value%");
    }


    private static function creditUniqueId()
    {
        do {
            $uniqueId = Str::random(10);
            $credit = \App\Models\Credit::whereUniqueId($uniqueId)->first();
        } while (!empty($credit));

        return $uniqueId;

    }

    public static function createRandomPin()
    {

        $min = pow(10, config("nasimMoshaver.pincodeDigitsCount", 8) - 1);
        $max = pow(10, config("nasimMoshaver.pincodeDigitsCount", 8)) - 1;
        $ignoreCount = 0;
        while ($ignoreCount < 1000) {
            $randomPin = rand($min, $max);
            $credit = Credit::whereIdentifier($randomPin)->first();
            if (!$credit)
                return $randomPin;

            $ignoreCount++;
        }

        return null;

    }

}
