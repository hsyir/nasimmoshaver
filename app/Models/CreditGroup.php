<?php

namespace App\Models;

use App\User;
use Baloot\EloquentHelper;
use Illuminate\Database\Eloquent\Model;

class CreditGroup extends Model
{
    use EloquentHelper;


    public function creator()
    {
        return $this->belongsTo(User::class, "creator_id")
            ->withDefault("none");
    }


    public function credits()
    {
        return $this->hasMany(Credit::class);
    }


}
