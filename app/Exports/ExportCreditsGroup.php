<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class ExportCreditsGroup implements FromView
{

    private $creditGroup;

    public function __construct($creditGroup)
    {
        $this->creditGroup = $creditGroup;
    }

    public function view(): View
    {
        return view("back.creditGroups.download",["creditGroup"=>$this->creditGroup]);
    }
}
