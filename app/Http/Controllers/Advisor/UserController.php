<?php

namespace App\Http\Controllers\Advisor;

use App\Classes\Simotel\SyncUsersInQueues;
use App\Http\Controllers\Controller;
use App\Models\Queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        $userQueues = $user->queues()->withCount("users")->get();
        $callsInQueuesReport = Queue::select(DB::raw(
            "queues.id, queues.name , sum(calls.call_time) as calls_time_total ,avg(calls.call_time) as calls_time_avg ,avg(calls.poll_score) as poll_score_avg "
        ))
            ->join("calls", "queues.id", "=", "calls.queue_id")->where("calls.user_id", $user->id)
            ->groupBy("queues.id", "queues.name")->orderBy("calls.call_time")->get()->dd();
    }


    public function changeStatus(Request $request)
    {
        $this->validate($request, [
            "active" => "required|boolean",
        ]);

        $pause = !$request->active;

        $user = Auth::user();
        $user->simotel_paused = $pause;
        $user->save();

        $simotel = new SyncUsersInQueues;

        if ($pause)
            $simotel->pauseUser($user);
        else
            $simotel->unpauseUser($user);


        return response()->json([
            "active" => !$pause
        ]);
    }

    public function schedules()
    {
        $user = Auth::user();
        return view("advisor.schedules.all", compact("user"));
    }
}
