<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Call;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CallController extends Controller
{
    public function index()
    {
        return view("back.calls.all");
    }

    public function getCalls()
    {
        $calls = Call::select(DB::raw("calls.* , users.name as user_name , queues.name as queue_name"))
            ->join("queues", "queues.id", "calls.queue_id")
            ->join("users", "users.id", "calls.user_id");

        return DataTables::of($calls)

            ->addColumn('user_name_str', function ($row) {
                return "<a href='" . route("admin.users.show", $row->user_id) . "'>$row->user_name</a>";
            })
            ->addColumn('queue_name_str', function ($row) {
                return "<a href='" . route("admin.queues.show", $row->queue_id) . "'>$row->queue_name</a>";
            })
            ->addColumn('created_at_fa_ftt', function ($row) {
                return "<a href='" . route("admin.calls.show", $row->id) . "'>$row->created_at_fa_ftt</a>";
            })
            ->addColumn('call_time_readable', function ($row) {
                return $row->call_time_readable_short;
            })
            ->addColumn('total_amount_formatted', function ($row) {
                return number_format($row->total_amount);
            })

            ->rawColumns(['user_name_str', "queue_name_str","created_at_fa_ftt"])
            ->make(true);
    }

    public function show()
    {

    }
}
