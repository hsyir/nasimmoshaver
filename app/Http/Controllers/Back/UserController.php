<?php

namespace App\Http\Controllers\Back;

use App\Classes\Reports\CallsReports;
use App\Http\Controllers\Controller;
use App\Models\Call;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::withCount('queues')->paginate(100);
        return view("back.users.all", compact("users"));
    }

    public function getUsers()
    {
        $query = User::select(DB::raw("users.*,count(`queue_user`.`id`) as queues_count"))
            ->leftJoin("queue_user", "users.id", "queue_user.user_id")
            ->groupBy("users.id");


        return DataTables::of($query)
            ->addColumn('user_name', function ($row) {
                return "<a href='" . route("admin.users.show", $row->id) . "'>$row->name</a>";
            })->addColumn('queues_count', function ($row) {
                return $row->queues_count;
            })
            ->rawColumns(['user_name'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $user = new User();
        return view("back.users.create", compact("user"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user = $request->isMethod("POST") ? new User : User::findOrFail($request->user_id);

        $this->validate($request, $this->validationRules($user));

        $user->name = $request->name;
        $user->email = $request->email;
        $user->simotel_number = $request->simotel_number;
        $user->mobile = $request->mobile;
        $user->entrance_fee = $request->entrance_fee;
        $user->credit_ratio = $request->credit_ratio;
        $user->address = $request->address;
        $user->comment = $request->comment;
        $user->level = $request->level;

        if ($request->password)
            $user->password = bcrypt($request->password);

        $user->save();
        $route = $request->isMethod("POST") ? route("admin.users.index") : route("admin.users.show", $user);
        return self::redirectWithSuccess($route, "اطلاعات ذخیره شد");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $report = new CallsReports();
        $report->setRowsPerPage(10);
        $callsReport = (object)$report->statisitcs(null, $user->id);
        $userLastCalls = $report->getCalls(null, $user->id);
        $userQueues = $user->queues()->withCount("users")->get();
        $callsInQueuesReport = Call::select(DB::raw(
            "queues.id, queues.name , sum(calls.call_time) as calls_time_total "
                . ",avg(calls.call_time) as calls_time_avg ,avg(calls.poll_score) as poll_score_avg "
                . ",sum(total_amount) as total_amount"
        ))
            ->join("queues", "queues.id", "=", "calls.queue_id")->where("calls.user_id", $user->id)
            ->groupBy("queues.id", "queues.name")->get();

        return view("back.users.show", compact("callsReport", "callsInQueuesReport", "userQueues", "user", "userLastCalls"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view("back.users.edit", compact("user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->queues()->detach();
        $user->schedules()->delete();
        $user->forcedelete();
        return $this->redirectWithSuccess(route("admin.users.index") ,"حذف شد");

    }

    public function archive(User $user)
    {
        ///soft delete
        $user->delete();
        return self::redirectWithSuccess(route("admin.users.archive.index"), "آرشیو شد");
    }

    public function recycle($user_id)
    {
        $user = User::withTrashed()->findOrFail($user_id);
        $user->restore();
        return self::redirectWithSuccess(route("admin.users.show", $user), "از آرشیو خارج شد");
    }

    public function archivedList()
    {
        $users = User::onlyTrashed()->orderBy("deleted_at", "DESC")->paginate(100);
        return view('back.users.trash', compact("users"));
    }



    /**
     * @param User $user
     * @return array
     */
    private function validationRules(User $user): array
    {
        $rules = [
            "name" => "required",
            "email" => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            "mobile" => "sometimes|nullable|iran_mobile|unique:users,mobile," . $user->id,
            "simotel_number" => "sometimes|nullable|unique:users,simotel_number," . $user->id,
            "password" => "sometimes|nullable|confirmed|min:6",
            "entrance_fee" => "required|integer",
            "credit_ratio" => "required|integer",
        ];
        return $rules;
    }

    public function scheduleForm(User $user)
    {
        return view("back.schedules.all", compact("user"));
    }
}
