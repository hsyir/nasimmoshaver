<?php

namespace App\Http\Controllers\Back;

use App\Classes\Reports\CallsAnalyze;
use App\Classes\Reports\CallsReports;
use App\Classes\Reports\LoggedInUsers;
use App\Http\Controllers\Controller;
use App\Models\Queue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $lastMonthCallsAnalyze = (new CallsAnalyze())->lastDaysReport();
        $allCallsAnalyze = (new CallsAnalyze())->first();

        $onlineUsers = (new LoggedInUsers())->users();

        $report = new LoggedInUsers();
//        $report->setDay(Carbon::now()->dayOfWeek);
        $report->setStartTime(Carbon::now()->format("H:i:s"));
        $report->setEndTime(Carbon::now()->addHours(2));
        $scheduledOnlineUsers = $report->users();

        $report = new CallsReports();
        $report->setRowsPerPage(20);
        $recentCalls = $report->getCalls();

        $queues = Queue::select(DB::raw(
            "queues.id, queues.name , sum(calls.call_time) as calls_time_total,sum(`calls`.`total_amount`) as `total_amount` ,avg(calls.call_time) as calls_time_avg ,avg(calls.poll_score) as poll_score_avg "
        ))
            ->join("calls", "queues.id", "=", "calls.queue_id")
            ->groupBy("queues.id", "queues.name")->get();


        return view(
            "back.dashboard.index",
            compact(
                "onlineUsers", "allCallsAnalyze", "queues",
                "recentCalls", "lastMonthCallsAnalyze", "scheduledOnlineUsers"
            )
        );
    }
}
