<?php

namespace App\Http\Controllers\Back;

use App\Exports\ExportCreditsGroup;
use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\CreditCharges;
use App\Models\CreditGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class CreditGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $creditGroups = CreditGroup::with("creator")->withCount("credits")->paginate(100);
        return view("back.creditGroups.all", compact("creditGroups"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("back.creditGroups.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => "required",
        ]);

        $creditGroup = new CreditGroup();
        $creditGroup->title = $request->title;
        $creditGroup->comment = $request->comment;
        $creditGroup->creator_id = Auth::user()->id;
        $creditGroup->save();

        return self::redirectWithSuccess(route("admin.creditGroups.show", $creditGroup), "ایجاد شد");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storePins(Request $request, CreditGroup $creditGroup)
    {
        $this->validate($request, [
            "pins_count" => "required|integer|min:1|max:1000",
            "credit_amount" => "required|integer",
        ]);


        $pins_count = $request->pins_count;
        $digits_count = config("nasimMoshaver.pincodeDigitsCount");
        $min = pow(10, $digits_count - 1);
        $max = pow(10, $digits_count) - 1;
        $creditPins = [];
        $i = 1;
        $ignoreCount = 0;
        while ($i <= $pins_count and $ignoreCount < 1000) {
            $randomPin = rand($min, $max);
            $credit = Credit::whereIdentifier($randomPin)->first();
            if (!$credit) {
                $creditPins[] = $randomPin;
                $i++;
                continue;
            }
            $ignoreCount++;
        }

        foreach ($creditPins as $pin) {
            $credit = new Credit();
            $credit->identifier = $pin;
            $credit->credit_group_id = $creditGroup->id;
            $credit->save();

            $credit->charges()->create([
                'amount' => $request->credit_amount,
            ]);

        }

        return self::redirectWithSuccess(route("admin.creditGroups.show", $creditGroup), "ایجاد شد");

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CreditGroup $creditGroup
     * @return \Illuminate\Http\Response
     */
    public function show(CreditGroup $creditGroup)
    {
        $creditGroup->load("credits", "creator");

        $chargesReport = CreditGroup::join("credits", "credits.credit_group_id", "=", "credit_groups.id")
            ->join("credit_charges", "credit_charges.credit_id", "=", "credits.id")
            ->where("credit_groups.id", $creditGroup->id)
            ->get(DB::raw("sum(`credit_charges`.`amount`)  as total "))
            ->first();

        $fields = [
            DB::raw("sum(`calls`.`total_amount`)  as total_amount"),
            DB::raw("sum(`calls`.`call_time`)  as call_time"),
            DB::raw("avg(`calls`.`poll_score`)  as poll_score"),
            DB::raw("count(`calls`.`id`)  as calls_count"),
        ];

        $usedChargesReport = CreditGroup::join("credits", "credits.credit_group_id", "=", "credit_groups.id")
            ->join("calls", "calls.credit_id", "=", "credits.id")
            ->where("credit_groups.id", $creditGroup->id)
            ->get($fields)
            ->first();


        return view("back.creditGroups.show", compact("creditGroup", "usedChargesReport", "chargesReport"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CreditGroup $creditGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(CreditGroup $creditGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param \App\Models\CreditGroup $creditGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreditGroup $creditGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\CreditGroup $creditGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreditGroup $creditGroup)
    {
        $credits = CreditCharges::whereIn("credit_id",
        $creditGroup
        ->credits()->get("id")
        ->pluck("id")->toArray())->delete();
        $creditGroup->delete();
        return self::redirectWithSuccess(route("admin.creditGroups.index"), "حذف شد");
    }


    public function download(CreditGroup $creditGroup)
    {
        return Excel::download(new ExportCreditsGroup($creditGroup), $creditGroup->id . ".csv");
    }


    public function getGreditsOfGroup(CreditGroup $creditGroup)
    {
        $query =
            Credit
                ::select(
                    DB::raw("`credits`.`id`,`identifier`,`phone_number`,sum(`credit_charges`.`amount`) as `sum_credits` , sum(`calls`.`total_amount`) as `total_usages` ")
                )
                ->join('credit_charges', 'credits.id', "=", "credit_charges.credit_id")
                ->leftJoin("calls", 'credits.id', '=', 'calls.credit_id')
                ->whereCreditGroupId($creditGroup->id)
                ->groupBy("credits.id");
        return DataTables::of($query)
            ->addColumn('identifier', function ($row) {
                return "<a href='".route("admin.credits.show",$row->id)."'>$row->identifier</a>";
            })
            ->addColumn('sum_credit', function ($row) {
                return number_format($row->sum_credits);
            })
            ->addColumn('total_usage', function ($row) {
                return number_format($row->total_usages);
            })
             ->addColumn('remain_credit', function ($row) {
                 return number_format($row->sum_credits - $row->total_usages);
             })
            ->rawColumns(['identifier'])
            ->make(true);
    }
}
