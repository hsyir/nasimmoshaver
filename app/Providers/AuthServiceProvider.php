<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::before(function ($user, $ability) {
            if ($user->isAdmin()) {
                return true;
            }
        });


        Gate::define("advisor-privilege",function($user){
             return $user->isAdvisor();
        });

        Gate::define("delete-schedule",function($user,$schedule){

             return $user->id == $schedule->user_id;
        });





    }
}
