<?php

namespace App\Providers;

use App\Listeners\ChangeSimotelUserStat;
use App\Listeners\ShowEventData;
use App\Listeners\UpdateCallCdrData;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Nasim\Simotel\Laravel\Events\SimotelEventCdr;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*        SimotelEventNewState::class => [
        //            ShowEventData::class,
        //            ChangeSimotelUserStat::class
                ],
                SimotelEventIncomingCall::class => [
                    ShowEventData::class,
                ],
                SimotelEventOutgoingCall::class => [
                    ShowEventData::class,
                ],*/
        SimotelEventCdr::class => [
//            ShowEventData::class,
            UpdateCallCdrData::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
