<?php

namespace App;

use App\Models\Queue;
use App\Models\Schedule;
use Baloot\EloquentHelper;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use EloquentHelper;

    const LEVEL_ADMIN = 3;
    const LEVEL_ADVISOR = 2;
    const LEVEL_USER = 1;

    const RESPONDING_STATUS_ACTIVE = 1;
    const RESPONDING_STATUS_PAUSE = 2;

    const SIMOTEL_STATUS_IDLE = 1;
    const SIMOTEL_STATUS_UNAVAILABLE = 2;
    const SIMOTEL_STATUS_BUSY = 2;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'mobile', 'simotel_number', 'comment',"level","credit_ratio","entrance_fee"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    public function scopeOrlike($query, $field, $value)
    {
        return $query->orWhere($field, 'LIKE', "%$value%");
    }


    public function queues()
    {
        return $this->belongsToMany(Queue::class);
    }


    public function isAdmin()
    {
        return $this->level == self::LEVEL_ADMIN;
    }

    public function isAdvisor()
    {
        return $this->level == self::LEVEL_ADVISOR;
    }


    public function schedules()
    {
        return $this->hasMany(Schedule::class)
            ->orderBy("day", "ASC")
            ->orderBy("start_time", "ASC");
    }


    public function findAdvisor($simotelNumber){
        return self::whereSimotelNumber($simotelNumber)->first();
    }

}
