<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 9/13/2020
 * Time: 12:11 PM
 */

namespace App\Classes\Simotel\SmartApiApps;

use App\Models\Call;
use App\Models\Queue;
use App\User;
use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class SelectQueue
{
    use SmartApiCommands;
    use MoshavereSmartTrait;

    public function selectQueue($appData): array
    {
        $appData=collect($appData);
        if (!$appData->has("data")) {
            $this->cmdExit("wrongQueue");
            return $this->okResponse();
        }

        $queue = Queue::whereAnnouncementId($appData->get("data"))->first();
        if (!$queue) {
            $this->cmdExit("wrongQueue");
            return $this->okResponse();
        }

        $call = $this->getCurrentCall();
        $call->user_id = null;
        $call->queue_id = $queue->id;
        $call->entrance_fee = $queue->entrance_fee;
        $call->credit_ratio = $queue->credit_ratio;
        $call->save();
        $credit = $call->credit;

        $creditTimeMinutes = $this->calculateCreditTime($credit->remain_credit, $queue->entrance_fee, $queue->credit_ratio);
        $creditTimePeriod = $this->minutesToTimePeriod($creditTimeMinutes);

        $this->cmdPlayAnnouncement("YourCredit");
        $this->cmdSayDuration($creditTimePeriod);
        $this->cmdSetExten($queue->simotel_number);
        $this->cmdSetLimitOnCall($creditTimeMinutes * 60);
        $this->cmdExit("toQueue");

        return $this->okResponse();
    }

    public function register_poll_score($appData): array
    {
        $appData=collect($appData);
        if (!$appData->has('data')) {
            $this->cmdExit("2");
            return $this->okResponse();
        }

        try {
            $call = Call::whereUniqueId($appData->get("unique_id"))->first();
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }

        $scoreData = $appData->get("data");

        if (!in_array($scoreData, $this->pollScores)) {
            $this->cmdExit("2");
            return $this->okResponse();
        }


        $call->poll_score = $scoreData;
        $call->save();

        $this->cmdExit(1);

        return $this->okResponse();
    }
}
