<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 9/13/2020
 * Time: 12:11 PM
 */

namespace App\Classes\Simotel\SmartApiApps;

use App\Models\Call;
use App\Models\Credit;
use App\Models\Queue;
use App\User;
use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;
use Illuminate\Support\Facades\Log;

class CheckPinCode
{
    use SmartApiCommands;
    use MoshavereSmartTrait;

    public function checkPinCode($appData): array
    {
        $appData=collect($appData);
        $pincode = $appData->get("data");
        
        $credit = Credit::whereIdentifier($pincode)->first();
        if (!$credit) {
            $this->cmdExit("wrongPincode");
            return $this->okResponse();
        }

        
        if ($credit->remain_credit < 5000) {
            $this->cmdExit("noCredit");
            return $this->okResponse();
        }

        $call = $this->getCurrentCall();
        $call->putCredit($credit);

        $this->cmdPlayAnnouncement("YourCredit");
        $this->cmdSayDuration($credit->remain_credit);
        $this->cmdPlayAnnouncement("Toman");
        $this->cmdExit("hasCredit");
        return $this->okResponse();
    }
}
