<?php


namespace App\Classes\Simotel\SmartApiApps;

use App\Models\Call;
use App\Models\Credit;
use Carbon\CarbonInterval;

trait MoshavereSmartTrait
{
    private $pollScores = [1, 2, 3, 4, 5];
    private $pollScoresDigitsCount = 1;
    private $pollScoresTimeout = 3;
    private $selectQueueDigitCounts = 1;
    private $selectQueueTimeout = 5;
    private $pincodeDigitsCount = 8;
    private $pincodeTimeout = 10;

    private $appData = [];
    private $selectAdvisorDigitsCount = 3;

    public function __construct()
    {
        $this->pollScores = config("nasimMoshaver.pollScores", $this->pollScores);
        $this->pollScoresDigitsCount = config("nasimMoshaver.pollScoresDigitsCount", $this->pollScoresDigitsCount);
        $this->pollScoresTimeout = config("nasimMoshaver.pollScoresTimeout", $this->pollScoresTimeout);
        $this->selectQueueDigitCounts = config("nasimMoshaver.selectQueueDigitCounts", $this->selectQueueDigitCounts);
        $this->selectQueueTimeout = config("nasimMoshaver.selectQueueTimeout", $this->selectQueueTimeout);
        $this->pincodeDigitsCount = config("nasimMoshaver.pincodeDigitsCount", $this->pincodeDigitsCount);
        $this->pincodeTimeout = config("nasimMoshaver.pincodeTimeout", $this->pincodeTimeout);
    }


    private function minutesToTimePeriod($creditTimeMinutes): string
    {
        $time = CarbonInterval::minutes($creditTimeMinutes)->cascade();
        return "$time->d.$time->h:$time->i:$time->s";
    }

    private function registerCallWithQueue($unique_id, $srcNumber, $queueId): Call
    {
        return $this->registerCall($unique_id, $srcNumber, null, $queueId);
    }

    private function registerCallWithUser($unique_id, $srcNumber, $userId): Call
    {
        return $this->registerCall($unique_id, $srcNumber, $userId, null);
    }
    /*
        private function registerCall($uniqueId, $srcNumber, $userResponderId = null, $queueId = null): Call
        {
            $call = Call::whereUniqueId($uniqueId)->first() ?? new Call;

            $call->unique_id = $uniqueId;
            $call->queue_id = $queueId;
            $call->user_id = $userResponderId;
            $call->src_number = $srcNumber;
            $call->save();
            return $call;
        }
     */
    private function identifierHasCredit($pin, $necessaryAmount)
    {
        $credit = Credit::whereIdentifier($pin)->first();

        if (!$credit) {
            return null;
        }

        if ($credit->remain_credit > $necessaryAmount + config("nasimMoshaver.minCreditToConnect")) {
            return $credit;
        }

        return false;
    }

    private function phoneNumberHasCredit($phone_number, $necessaryAmount)
    {
        $credits = Credit::wherePhoneNumber($phone_number)->get();

        if ($credits->count() < 1) {
            return null;
        }

        foreach ($credits as $credit) {
            if ($credit->remain_credit > $necessaryAmount + config("nasimMoshaver.minCreditToConnect")) {
                return $credit;
            }
        }

        return false;
    }

    public function calculateCreditTime($credit, $entranceFee, $creditRatio)
    {
        return (integer)(($credit - $entranceFee) / $creditRatio);
    }


    public function getCurrentCall()
    {
        return Call::whereUniqueId(request()->get("unique_id"))->first();
    }
}
