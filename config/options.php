<?php
return [
    'optionsModel' => \Hsy\Options\Models\Option::class,
    'groups' =>
        [
            'site-options' => [
                'title' => 'تنظیمات وب سایت',
                'fields' => [
                    [
                        'key' => 'site-name',
                        'title' => 'نام سایت',
                        'type' => 'text',
                        'description' => '',
                    ],
                    [
                        'key' => 'zarinpal-mode',
                        'title' => 'نام سایت',
                        'type' => 'text',
                        'description' => '',
                    ],
                    [
                        'key' => 'zarinpal-merchant',
                        'title' => 'نام سایت',
                        'type' => 'text',
                        'description' => '',
                    ],
                    [
                        'key' => 'credit-plans',
                        'type' => 'text',
                        'description' => '',
                    ],
                ]
            ],
        ]
];
