<?php
return [

    "views_path"=>"html::components.",

    'components' => [
        'text', 'hidden', 'checkbox',
        'switch', 'select', 'textarea',
        'file', 'method', 'submit',
        'image', 'form', 'endform',
        'info'
    ],

    'variables' => [
        'name', 'method', 'description',
        'label', "class", 'value','checked',"select_options"
    ],
];
