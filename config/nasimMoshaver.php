<?php

return [

    "syncWithSimotel" => false,

    /*
     *  Minimum necessary credit to start call
     */
    "minCreditToConnect" => 1000,

    "pollScores" => [1, 2, 3, 4, 5],
    "pollScoresDigitsCount" => 1,
    "pollScoresTimeout" => 10,

    "selectQueueDigitCounts" => 1,
    "selectQueueTimeout" => 15,


    /*
     *  pincodeDigitsCount max 16
     */
    "pincodeDigitsCount" => 8,
    "pincodeTimeout" => 20,

    "e-creditGroupId" => 1,


    "schedules" => [

        /*
         *  Minutes
         */
        "minimumRespondingTime" => 10,
    ],


    "settings" => [
        "fields" => [
            "minCreditToConnect" => [
                "title" => "حداقل اعتبار مکالمه",
                "description" => "",
                "type" => "text",
                "default" => 1000,
                "group" => 1,
            ],
            "selectQueueDigitCounts" => [
                "title" => "تعداد دیجیت انتخاب صف",
                "description" => "",
                "type" => "text",
                "default" => 1,
                "group" => 1,
            ],
            "selectQueueTimeout" => [
                "title" => " تایم اوت انتخاب صف",
                "description" => "",
                "type" => "text",
                "default" => 5,
                "group" => 1,
            ],
            "pincodeDigitsCount" => [
                "title" => "تعداد دیجیت پینکد",
                "description" => "",
                "type" => "text",
                "default" => 6,
                "group" => 1,
            ],
            "pincodeTimeout" => [
                "title" => " تایم اوت وارد کردن پیندکد",
                "description" => "",
                "type" => "text",
                "default" => 5,
                "group" => 1,
            ],
        ],
        "groups" => [
            1 => [
                "title" => "تنظیمات سیموتل",
                "icon" => ""
            ]
        ],
        "validation_rules" => [

        ]
    ]


];
