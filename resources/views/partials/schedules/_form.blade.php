<form action="{{ route('advisor.schedules.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('user_id')->value($user_id) }}
    {{ Html::hidden()->name('schedule_id')->value($schedule->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    @if($errors->has("conflict"))
                        <div class="alert alert-danger">
                            ثبت نشد، تداخل زمانی دارد.
                        </div>
                    @endif

                    {{
                    Html::text("start_time")
                    ->value(old("start_time",$schedule->start_time))
                    ->label("زمان شروع")
                    ->description("مثل: 07:30")
                     }}
                    {{
                    Html::text("end_time")
                    ->value(old("end_time",$schedule->end_time))
                    ->label("زمان پایان")
                    ->description("حداقل بازه زمانی: ".config("nasimMoshaver.schedules.minimumRespondingTime")." دقیقه")
                     }}
                    {{
                    Html::select("day")
                    ->select_options(
                        [
                            1=>"شنبه",
                            2=>"یک شنبه",
                            3=>"دو شنبه",
                            4=>"سه شنبه",
                            5=>"چهار شنبه",
                            6=>"پنج شنبه",
                            7=>"جمعه",
                        ]
                    )
                    ->value(old("day",$schedule->day))
                    ->label("روز هفته")
                    ->description("")
                     }}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>
