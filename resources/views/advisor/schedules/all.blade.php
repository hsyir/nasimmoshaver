@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["Dashboard" =>route("advisor.dashboard.index"),"current"=>"برنامه زمانی حضور من"]])
    @endcomponent
@endsection

@section("content")
   @include("partials.schedules.all")
@endsection
