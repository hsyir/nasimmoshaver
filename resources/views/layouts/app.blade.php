<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{  \Options::group("site-options")->get("site-name","سامانه مدیریت مشاوره تلفنی") }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('front/css/app.css') }}" rel="stylesheet">

    @stack("styles")

</head>
<body class="rtl bg6 th2s">
<div id="app" class="">
    <nav class=" navbar navbar-expand-md navbar-light bg-white shadow-sm fixed-top" >
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{  \Options::group("site-options")->get("site-name","سامانه مدیریت مشاوره تلفنی") }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">ورود</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">ثبت نام</a>
                            </li>
                        @endif
                    @else

                        <li class="nav-item">
                            @can("advisor-privilege")
                                <a class="nav-link" href="{{ route('advisor.dashboard.index') }}">پنل کاربری</a>
                            @else
                                <a class="nav-link" href="{{ route('admin.dashboard.index') }}">پنل مدیر</a>
                            @endcan
                        </li>



                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    خروج
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <main class="">
        @yield('content')
    </main>
</div>
<!-- Scripts -->
<script src="{{ asset('front/js/app.js') }}" ></script>

@stack("scripts")
</body>

</html>
