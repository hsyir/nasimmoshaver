@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
["items"=> ["Dashboard" =>route("admin.dashboard.index"),$user->name=>route("admin.users.show",$user),"current"=>"برنامه زمانی حضور مشاور"]])
    @endcomponent
@endsection

@section("content")
   @include("partials.schedules.all")
@endsection
