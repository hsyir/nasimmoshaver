@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),$credit->creditGroup->title =>route("admin.creditGroups.show",$credit->creditGroup),"current"=>"مشاهده شناسه اعتبار"]])
    @endcomponent
@endsection
@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>
    <div class="row">
        <div class="col-md-6 mb-4">
            <div class="card ">
                <div class="card-header">
                    اطلاعات شناسه اعتبار
                </div>
                <div class="card-body">
                    {{ Html::info()->value($credit->identifier)->label("شناسه")->icon("key") }}
                    {{ Html::info()->value($credit->phone_number)->label("شماره تلفن اختصاصی")->icon("") }}
                    {{ Html::info()->value(number_format($credit->total_credit)." تومان ")->label("مجموع مبلغ شارژ شده")->icon("coins") }}
                    {{ Html::info()->value(number_format($credit->total_usage)." تومان ")->label("مجموع اعتبار کسر شده")->icon("donate") }}
                    {{ Html::info()->value(number_format($credit->remain_credit) ." تومان ")->label("مانده اعتبار")->icon("money-check-alt") }}
                    {{ Html::info()->value("")->label("مجموع زمان مکالمات")->icon("business-time") }}


                </div>

                <div class="card-footer">
                    <div class="float-left">
                      {{--   <a class="btn btn-outline-success btn-sm" href="{{ route("admin.users.edit",$user) }}"><i
                                class="fa fa-edit"></i> ویرایش مشخصات </a>
                                <a class="btn btn-warning btn-sm" href="{{ route("admin.users.archive",$user) }}"><i
                                class="fa fa-recycle"></i> آرشیو</a>
                                <a class="btn btn-danger btn-sm confirmation" href="{{ route("admin.users.destroy",$user) }}"><i
                                class="fa fa-trash"></i> حذف دائم</a> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-4">
            <div class="card ">
                <div class="card-header">لینک یکتا</div>
                <div class="card-body text-left">
                    @if($credit->unique_id)
                        <div>
                            <a href="{{ $credit->unique_url }}" target="_blank">{{ $credit->unique_url }}</a>
                        </div>
                        <div>
                            <a href="{{ route("admin.credits.createUniqueUrl",$credit)}}"
                               class="btn btn-sm btn-outline-success">تولید لینک جدید</a>
                        </div>
                    @else
                        تا کنون برای این کارت اعتباری لینک یکتا ایجاد نشده است.
                        <div>
                            <a href="{{ route("admin.credits.createUniqueUrl",$credit)}}"
                               class="btn btn-sm btn-outline-success">تولید لینک جدید</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">افزایش اعتبار</div>

                <div class="card-body">
                    <form action="{{ route("admin.credits.addCharge",$credit) }}" method="post">
                        @csrf


                        <div class="row">
                            <div class="col-md-12">
                                {{
                       Html::text("amount")
                       ->value(old("amount",$credit->amount))
                       ->label("مبلغ اعتبار")
                       ->description("به تومان")
                        }}
                            </div>
                            <div class="col-md-12">
                                {{
                       Html::text("comment")
                       ->value(old("comment"))
                       ->label("توضیحات")
                       ->description("در صورت نیاز به توضیح")
                        }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">{{ Html::submit()->label('افزودن اعتبار') }}</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-4">
            <div class="card ">
                <div class="card-header">
                    شارژ های انجام شده
                </div>
                <div class="card-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>مقدار اعتبار</th>
                            <th>زمان ایجاد</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($credit->charges as $charge)
                            <tr>
                                <td>{{ $charge->amount_readable }}</td>
                                <td>{{ $charge->created_at_fa_ft }}</td>
                                <td>{{ $charge->comment }}</td>
                                <td>{{ $charge->creator->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card ">
                <div class="card-header">تاریخچه تماس ها</div>
                <div class="card-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>شماره ورودی</th>
                            <th>هزینه ورودی</th>
                            <th>هزینه هر دقیقه</th>
                            <th>مدت تماس</th>
                            <th>هزینه تماس</th>
                            <th>جمع هزینه</th>
                            <th>صف</th>
                            <th>مشاور پاسخگو</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($credit->calls as $call)
                            <tr>
                                <td>{{ $call->src_number }}</td>
                                <td>{{ number_format($call->entrance_fee) }}</td>
                                <td>{{ number_format($call->credit_ratio) }}</td>
                                <td>{{ $call->call_time_readable }}</td>
                                <td>{{ number_format($call->call_amount) }}</td>
                                <td>{{ number_format($call->total_amount) }}</td>
                                <td>{{ $call->queue->name }}</td>
                                <td>{{ $call->user->name }}</td>
                                <td>{{ $call->created_at_fa_ft }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ number_format($credit->total_usage) }} تومان</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        {{-- <a class="btn btn-outline-success btn-sm" href="{{ route("admin.users.edit",$user) }}"><i
                                 class="fa fa-edit"></i> ویرایش مشخصات </a>
                         <a class="btn btn-outline-success btn-sm" href="{{ route("admin.users.archive",$user) }}"> آرشیو
                             <i class="fa fa-archive"></i> </a>--}}
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
