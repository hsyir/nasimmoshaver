@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"Users" =>route("admin.users.index"),"current"=>"تعریف کاربر جدید"]])
    @endcomponent
@endsection

@section("content")
    <x-success></x-success>
    <x-errors></x-errors>
        @include("back.users._form",["action"=>"create"])
@endsection
