@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["Dashboard" =>route("admin.dashboard.index"),"current"=>"کاربران"]])
        <a href="{{ route("admin.users.create") }}" class="btn btn-info btn-sm">جدید</a>
    @endcomponent
@endsection

@section("content")
    <x-success></x-success>
    <div class="card">
        <div class="card-body">
            <table class="table usersTable">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>ایمیل</th>
                    <th>شماره سیموتل</th>
                    <th>تعداد صف</th>
                    <th>شماره موبایل</th>
                    <th>توضیجات</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            var table = $('.usersTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.users.getUsers') }}",
                columns: [
                    {data: 'user_name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'simotel_number', name: 'simotel_number'},
                    {data: 'queues_count', name: 'queues_count'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'comment', name: 'comment'},
                ]
            });

        });
    </script>

@endpush

