@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"Users" =>route("admin.users.index"),"current"=>"مشاهده وضعیت کاربر"]])
    @endcomponent
@endsection
@section("content")
    <style>
        .card-footer.form {
            background-color: rgba(53, 29, 2, 0.3) !important;
        }
    </style>

    <x-success></x-success>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-4 col-xs-offset-3 col-lg-4 col-xl-2">
                            <img src="/images/avatar-placeholder.png" alt="" class="w-100 rounded-circle">
                        </div>
                        <div class="col-md-10 col-lg-8 col-xl-10">
                            <div class="text-success h4"><i class="fa fa-user"></i> {{ $user->name }} </div>
                            {{ Html::info()->value($user->mobile)->label("شماره موبایل")->icon("mobile") }}
                            {{ Html::info()->value($user->simotel_number)->label("داخلی سیموتل")->icon("phone") }}
                            {{ Html::info()->value($user->comment)->label("توضیحات")->icon("info") }}
                            {{ Html::info()->value($user->created_at_fa_ftt)->label("زمان ثبت نام کاربر")->icon("plus") }}
                            {{ Html::info()->value($user->updated_at_fa_ftt)->label("زمان آخرین ویرایش")->icon("edit") }}
                            {{ Html::info()->value($user->simotel_paused ? "فعال" : "در استراحت")->label("وضعیت کاربر (استراحت)")->icon("user") }}
                            {{ Html::info()->value($user->credit_ratio)->label("ضریب کسر اعتبار")->icon("dollar") }}
                            {{ Html::info()->value($user->entrance_fee)->label("هزینه ورودی")->icon("money") }}


                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-success btn-sm" href="{{ route("admin.users.edit",$user) }}"><i
                                class="fa fa-edit"></i> ویرایش مشخصات </a>
                                <a class="btn btn-warning btn-sm" href="{{ route("admin.users.archive",$user) }}"><i
                                class="fa fa-recycle"></i> آرشیو</a>
                                <a class="btn btn-danger btn-sm confirmation" href="{{ route("admin.users.destroy",$user) }}"><i
                                class="fa fa-trash"></i> حذف دائم</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">

                <div class="col-md-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">کل زمان مکالمات</span>
                            <span class="info-box-number">{{ $callsReport->call_time_readable_short }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-star"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">میانگین امتیاز</span>
                            <span class="info-box-number">{{ round($callsReport->poll_score_avg,1) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-secondary"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">تعداد تماس ها</span>
                            <span class="info-box-number">{{ $callsReport->calls_count }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">میانگین زمان مکالمات</span>
                            <span class="info-box-number">{{ $callsReport->average_call_time_readable }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning"><i class="fa fa-coins"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">درآمد</span>
                            <span class="info-box-number">{{ $callsReport->total_amount }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">صف های فعال</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>نام صف</th>
                            <th>تعداد مشاور</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userQueues as $queue)
                            <tr>
                                <td>{{ $queue->name }}</td>
                                <td>{{ $queue->users_count }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">گزارش مکالمات در صف ها</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>نام صف</th>
                            <th>مجموع زمان مکالمه</th>
                            <th>میانگین زمان مکالمه</th>
                            <th>میتگین امتیاز</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($callsInQueuesReport as $report)
                            <tr>
                                <td>{{ $report->name }}</td>
                                <td>{{ secondsToShortTime($report->calls_time_total) }}</td>
                                <td>{{ secondsToShortTime($report->calls_time_avg) }}</td>
                                <td>{{ round($report->poll_score_avg,1) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">آخرین تماس ها</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>شناسه مکالمه</th>
                            <th>صف</th>
                            <th>مدت مکالمه</th>
                            <th>امتیاز</th>
                            <th>زمان شروع</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userLastCalls as $call)
                            <tr>
                                <td>{{ $call->id }}</td>
                                <td>{{ $call->queue->name }}</td>
                                <td>{{ $call->call_time_readable_short }}</td>
                                <td>{{ $call->poll_score }}</td>
                                <td>{{ $call->created_at_fa_ftt }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="card">
                <div class="card-header">برنامه حضور
                    <a href="{{ route("admin.users.schedule.form",$user) }}" class="btn btn-sm btn-outline-primary float-left">
                        تنظیم برنامه
                    </a>
                </div>

                <div class="card-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>روز هفته</th>
                            <th>شروع</th>
                            <th>پایان</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->schedules as $schedule)
                            <tr>
                                <td>{{ $schedule->day_readable }}</td>
                                <td>{{ $schedule->start_time_formatted }}</td>
                                <td>{{ $schedule->end_time_formatted }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>


    </div>

@endsection
@section("adminlte_js")
<script>
    $(document).ready(function(){
        $(".need_confirm").click(function(e){
            if(!confirm("آیا مطمئن هستید؟"))
            {
                e.preventDefault();
            }
        })
    })
</script>
@endsection
