@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"current"=>"لیست تماس ها"]])
    @endcomponent
@php
    use Hsy\Html\Facades\Html;
@endphp
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <table class="table text-left table-striped table-bordered callsTable">
                            <thead>
                            <tr >
                                <th>پاسخگو</th>
                                <th>صف</th>
                                <th>شماره مبدا</th>
                                <th>مدت تماس</th>
                                <th>مبلغ تماس</th>
                                <th>وضعیت</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            var table = $('.callsTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.calls.getCalls') }}",
                columns: [
                    {data: 'user_name_str', name: 'users.name'},
                    {data: 'queue_name_str', name: 'queues.name'},
                    {data: 'src_number', name: 'src_number'},
                    {data: 'call_time_readable', name: 'call_time'},
                    {data: 'total_amount_formatted', name: 'total_amount'},
                    {data: 'disposition', name: 'disposition'},
                    {data: 'created_at_fa_ftt', name: 'created_at'},
                ]
            });

        });
    </script>

@endpush
