<form action="{{ route('admin.queues.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('queue_id')->value($queue->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{
                    Html::text("name")
                    ->value(old("name",$queue->name))
                    ->label("عنوان صف")
                    ->description("جهت نمایش در نرم افزار مشاور")
                     }}
                    {{
                    Html::text("announcement_id")
                    ->value(old("announcement_id",$queue->announcement_id))
                    ->label("شناسه معرفی صف")
                    ->description("شناسه اعلام شده توسط آوای انتخاب صف در نقشه تماس سیموتل")
                     }}
                    {{
                    Html::text("simotel_number")
                    ->value(old("simotel_number",$queue->simotel_number))
                    ->label("شماره داخلی سیموتل")
                     }}
                    {{
                    Html::text("credit_ratio")
                    ->value(old("credit_ratio",$queue->credit_ratio))
                    ->label("ضریب کسر اعتبار")
                    ->description("کسر هزینه(تومان) به ازای هر دقیقه مکالمه")
                     }}
                    {{
                    Html::text("entrance_fee")
                    ->value(old("entrance_fee",$queue->entrance_fee)  ?? 0)
                    ->label("هزینه ورودی")
                    ->description("به تومان")
                     }}
                    {{ Html::text("comment")
                        ->value(old("comment",$queue->comment))
                        ->label("توضیحات")
                        ->description("")
                    }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                    </div>
                </div>
                </div>
            </div>

        </div>
    </div>
</form>
