@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"Queues" =>route("admin.queues.index"),"current"=>"تعریف صف جدید"]])
    @endcomponent
@endsection

@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>
    <x-errors></x-errors>
        @include("back.queues._form",["action"=>"create"])
@endsection
