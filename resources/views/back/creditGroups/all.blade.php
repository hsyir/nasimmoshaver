@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["Dashboard" =>route("admin.dashboard.index"),"current"=>"گروه های کارت اعتباری"]])
        <a href="{{ route("admin.creditGroups.create") }}" class="btn btn-info btn-sm">ایجاد گروه شارژ کارت</a>
    @endcomponent
@endsection

@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>
    <div class="card">
        <div class="card-header">تماس ها</div>

        <div class="card-body">
            <table class="table ">
                <thead>
                <tr>
                    <th>عنوان</th>
                    <th>تعداد پین</th>
                    <th>تاریخ ایجاد</th>
                    <th>توسط</th>
                </tr>
                </thead>
                <tbody>
                @foreach($creditGroups as $group)
                    <tr>
                        <td><a href="{{ route("admin.creditGroups.show",$group) }}">{{ $group->title }}</a></td>
                        <td>{{ number_format($group->credits_count) }}</td>
                        <td>{{ $group->created_at_fa_ftt }}</td>
                        <td>
                            @if($group->creator->id)
                            <a href="{{ route("admin.users.show",$group->creator->id) }}">{{ $group->creator->name }}</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
