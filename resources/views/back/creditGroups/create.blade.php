@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"CreditGroups" =>route("admin.creditGroups.index"),"current"=>"ایجاد گروه کارت شارژ"]])
    @endcomponent
@endsection

@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>

    <form action="{{ route('admin.creditGroups.store')  }}"
          method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{
                      Html::text("title")
                      ->label("عنوان")
                      ->value(old("title"))
                      ->description("")
                       }}
                        {{
                        Html::text("comment")
                        ->label("توضیح")
                        ->value(old("comment"))
                        ->description("")
                         }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">{{ Html::submit()->label('ایجاد') }}</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection
