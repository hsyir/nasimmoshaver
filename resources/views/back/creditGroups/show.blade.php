@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"CreditGroups" =>route("admin.creditGroups.index"),"current"=>"گروه کارت اعتبار"]])

    @endcomponent
@endsection
@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>

    <div class="row">
        <div class="col-md-4 mb-4">
            <div class="card ">
                <div class="card-header">
                    اطلاعات گروه اعتبار
                </div>
                <div class="card-body">
                    {{ Html::info()->value($creditGroup->title)->label("عنوان")->icon("title") }}
                    {{ Html::info()->value($creditGroup->comment)->label("توضیح")->icon("comment") }}
                    {{ Html::info()->value($creditGroup->creator->name)->label("ایجاد کننده")->icon("user") }}
                    {{ Html::info()->value($creditGroup->created_at_fa_ftt)->label("زمان ایجاد")->icon("business-time") }}
                    {{ Html::info()->value($creditGroup->credits->count())->label("تعداد کارت اعتباری ایجاد شده")->icon("digits") }}
                </div>
                <div class="card-footer">
                    <div class="float-left">

                                <a class="btn btn-danger btn-sm confirmation" href="{{ route("admin.creditGroups.destroy",$creditGroup) }}"><i
                                class="fa fa-trash"></i> حذف </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">مجموع اعتبار گروه</span>
                            <span class="info-box-number">{{ number_format($chargesReport->total) }} تومان</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">کل شارژ استفاده شده</span>
                            <span class="info-box-number">{{ $usedChargesReport->total_amount }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-star"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">میانگین امتیاز</span>
                            <span class="info-box-number">{{ round($usedChargesReport->poll_score,1) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-secondary"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">تعداد تماس ها</span>
                            <span class="info-box-number">{{ $usedChargesReport->calls_count }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل زمان مکالمات</span>
                            <span
                                class="info-box-number">{{ secondsToShortTime($usedChargesReport->call_time) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->


                </div>

            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    ایجاد گروهی پین
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.creditGroups.storePins',$creditGroup)  }}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        {{
                        Html::text("pins_count")
                        ->value(old("pins_count"))
                        ->label("تعداد پینکد های درخواستی")
                        ->description("حداکثر 1000 پینکد")
                         }}
                        {{
                        Html::text("credit_amount")
                        ->value(old("credit_amount"))
                        ->label("مبلغ اعتبار")
                        ->description("به تومان")
                         }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">{{ Html::submit()->label('ایجاد') }}</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    کد های اعتباری این گروه
                    <div class="float-left">
                        <a class="btn btn-outline-success btn-sm"
                           href="{{ route("admin.creditGroups.download",$creditGroup) }}"><i class="fa fa-edit"></i>
                            دانلود فایل اکسل </a>
                        {{--<a class="btn btn-outline-success btn-sm" href="{{ route("admin.users.archive",$user) }}"> آرشیو
                            <i class="fa fa-archive"></i> </a>--}}
                    </div>
                </div>
                <div class="card-body">

                    <table class="table creditsOfGroup ">
                        <thead>
                        <tr>
                            <th>شناسه</th>
                            <th>شماره تماس ورودی</th>
                            <th>مجموع اعتبار</th>
                            <th>اعتبار استفاده شده</th>
                            <th>مانده اعتبار</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            var table = $('.creditsOfGroup').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.creditGroups.getCreditsOfGroup',$creditGroup) }}",
                columns: [
                    {data: 'identifier', name: 'identifier'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'sum_credit', name: 'sum_credit'},
                    {data: 'total_usage', name: 'total_usage'},
                    {data: 'remain_credit', name: 'remain_credit'},
                ]
            });

        });
    </script>

@endpush
