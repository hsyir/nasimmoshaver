@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["current"=>"داشبورد ادمین"]])
    @endcomponent
@endsection

@php
    use Hsy\Html\Facades\Html;
@endphp
@section("content")
    <x-success></x-success>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">کل زمان مکالمات</span>
                            <span class="info-box-number">{{ $allCallsAnalyze->call_time_readable_short }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-star"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">میانگین امتیاز</span>
                            <span class="info-box-number">{{ round($allCallsAnalyze->poll_score_avg,1) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-secondary"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">تعداد تماس ها</span>
                            <span class="info-box-number">{{ $allCallsAnalyze->calls_count }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-phone"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">میانگین زمان مکالمات</span>
                            <span
                                    class="info-box-number">{{ secondsToShortTime($allCallsAnalyze->average_call_time) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->


                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-coins"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل درامد تماس ها</span>
                            <span class="info-box-number">{{ number_format($allCallsAnalyze->total_amount) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">تعداد کاربران سیستم</span>
                            <span class="info-box-number">{{ \App\User::all()->count() }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">تعداد کاربران آنلاین</span>
                            <span class="info-box-number">{{ $onlineUsers->count() }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-list"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">تعداد صف ها</span>
                            <span class="info-box-number">{{ \App\Models\Queue::all()->count() }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    کاربران آنلاین
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>شماره سیموتل</th>
                            <th>تعداد صف</th>
                            <th>وضعیت استراحت</th>
                            <th>تاریخ عضویت در سیستم</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($onlineUsers as $user)
                            <tr>
                                <td><a href="{{ route("admin.users.show",$user) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->simotel_number }}</td>
                                <td>{{ $user->queues_count }}</td>
                                <td>{{ $user->simotel_paused ?"استراحت" : "---" }}</td>
                                <td>{{ $user->created_at_fa_f }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    کاربران آنلاین در دو ساعت آینده
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>شماره سیموتل</th>
                            <th>تعداد صف</th>
                            <th>وضعیت استراحت</th>
                            <th>تاریخ عضویت در سیستم</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($scheduledOnlineUsers as $user)
                            <tr>
                                <td><a href="{{ route("admin.users.show",$user) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->simotel_number }}</td>
                                <td>{{ $user->queues_count }}</td>
                                <td>{{ $user->simotel_paused ?"استراحت" : "---" }}</td>
                                <td>{{ $user->created_at_fa_f }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">تحلیل تماس های ماه گذشته</div>
                <div class="card-body">
                    <div id="container" style="width:100%; height:400px;"></div>
                    <div id="container2" style="width:100%; height:400px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">آخرین تماس ها</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>شناسه مکالمه</th>
                            <th>صف</th>
                            <th>مشاور</th>
                            <th>مدت مکالمه</th>
                            <th>امتیاز</th>
                            <th>زمان شروع</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($recentCalls as $call)
                            <tr>
                                <td>{{ $call->id }}</td>
                                <td>
                                    @if($call->queue->id)
                                        <a href="{{ route("admin.queues.show",$call->queue) }}">{{ $call->queue->name }}</a>
                                    @else
                                        {{ $call->queue->name }}
                                    @endif
                                </td>
                                <td>
                                    @if($call->user->id)
                                        <a href="{{ route("admin.users.show",$call->user) }}">{{ $call->user->name }}</a>
                                    @else
                                        {{ $call->user->name }}
                                    @endif
                                </td>
                                <td>{{ $call->call_time_readable_short }}</td>
                                <td>{{ $call->poll_score }}</td>
                                <td>{{ $call->created_at_fa_ftt }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">صف ها</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>صف</th>
                            <th>مدت مکالمات</th>
                            <th>میانگین مکالمات</th>
                            <th>درامد</th>
                            <th>امتیاز</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($queues as $queue)
                            <tr>
                                <td><a href="{{ route("admin.queues.show",$queue) }}">{{ $queue->name }}</a></td>
                                <td>{{ secondsToShortTime($queue->calls_time_total) }}</td>
                                <td>{{ secondsToShortTime($queue->calls_time_avg) }}</td>
                                <td>{{ number_format($queue->total_amount) }}</td>
                                <td>{{ round($queue->poll_score_avg,2) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@push("js")
    <script defer>
        $("document").ready(function () {
            var myChart = Highcharts.chart('container', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: '  نمودار تماس ها در یک ماه اخیر'
                },
                xAxis: {
                    categories: [@foreach($lastMonthCallsAnalyze->keyBy("create_date") as $day => $analyze) '{{ $analyze->create_date_fa_f }}', @endforeach ]
                },
                yAxis: {
                    title: {
                        text: 'تعداد تماس'
                    }
                },
                series: [{
                    name: 'تماس ها',
                    data: [@foreach($lastMonthCallsAnalyze->keyBy("create_date") as $day => $analyze) {{ $analyze->calls_count }}, @endforeach]
                },]
            });

            var myChart2 = Highcharts.chart('container2', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: '  نمودار درامد ها در یک ماه اخیر'
                },
                xAxis: {
                    categories: [@foreach($lastMonthCallsAnalyze->keyBy("create_date") as $day => $analyze) '{{ $analyze->create_date_fa_f }}', @endforeach ]
                },
                yAxis: {
                    title: {
                        text: 'میزان دارمد'
                    }
                },
                series: [{
                    name: 'مبلغ',
                    data: [@foreach($lastMonthCallsAnalyze->keyBy("create_date") as $day => $analyze) {{ $analyze->total_amount }}, @endforeach]
                },]
            });
        })
    </script>
@endpush
