@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["Dashboard" =>route("admin.dashboard.index"),"current"=>"تنظیمات"]])
    @endcomponent
@endsection

@section("content")
    <x-success></x-success>
    <x-errors></x-errors>

    <form action="{{ route("admin.options.store") }}" method="post">
        @csrf
    <div class="card">
        <div class="card-header">
            تنظیمات وب سایت
        </div>
        <div class="card-body">
                {{ Html::text("site-name")->label("عنوان وب سایت")->value($option = Options::group("site-options")->get("site-name")) }}
                {{ Html::text("zarinpal-mode")->label("مود زرین پال")->value($option = Options::group("site-options")->get("zarinpal-mode"))->description("sandbox, normal, zaringate") }}
                {{ Html::text("zarinpal-merchant")->label("مرچنت زرین پال")->value($option = Options::group("site-options")->get("zarinpal-merchant")) }}
        </div>
    </div>
    <div class="card">
        <div class="card-header">
           خرید آنلاین شارژ
        </div>
        <div class="card-body">


            @php
                $plans = Options::group("site-options")->get("credit-plans");
                if($plans!=null){
                    $plans =  explode(",",Options::group("site-options")->get("credit-plans"));
                }
                else {
                    $plans = [];
                }
                
            @endphp
                <h4 class="h4">لیست مبالغ شارژ کارت ها</h4>
                <credit-plans :credit_plans={{json_encode($plans) }}></credit-plans>
        </div>
    </div>
<div class="row mb-5">
    <div class="col mb-5">
        <input type="submit" value="ذخیره" class="btn btn-primary">
    </div>
</div>
</form>
@endsection


