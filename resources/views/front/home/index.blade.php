@extends('layouts.app')

@section('content')

    <div class="home-page pb-5">
        <div class="container purchase-form mb-5 mt-3">
            <div class="row">

            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="{{ route("front.purchase") }}" method="post">
                        <div class="card ">
                            <div class="card-header bg-info text-white"><h2 class="h5">خرید کارت اعتباری مشاوره</h2>
                            </div>
                            <div class="card-body">
                                @csrf

                                {{
                                       Html::text("customer_name")
                                       ->label("نام و نام خانوادگی:")
                                       ->value(old("customer_name"))
                                }}

                                {{
                                    Html::text("customer_mobile")
                                    ->label("شماره موبایل:")
                                    ->value(old("customer_mobile"))
                                }}
                                @php
                                $plans = Options::group("site-options")->get("credit-plans","[]");
                                $plans = explode(",",$plans);
                                foreach ($plans as $value) {
                                    $plansArray[$value] = number_format((int)$value) . "تومان";
                                }
                                @endphp
                                    {{
                                    Html::select("amount")
                                    ->label("مبلغ اعتبار مورد نیاز:")
                                    ->value(old("amount"))
                                    ->description("")
                                    ->select_options($plansArray)
                                }}

                                {{
                                Html::text("customer_phone_number")
                                ->label("شماره تماس ورودی:")
                                ->value(old("customer_phone_number"))
                                ->description("لطفا شماره مورد نظر خود را به دقت و به صورت کامل وارد کنید. مثل: 09150000000 یا 02133215252")
                                }}
                            </div>
                            <div class="card-footer bg-info">

                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="submit" value=" پرداخت" class="btn btn-outline-light float-left">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
