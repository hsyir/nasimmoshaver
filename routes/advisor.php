<?php

use Illuminate\Support\Facades\Route;



Route::middleware("auth")->prefix("advisor")->as("advisor.")
    ->group(function () {

        Route::delete("schedules/{schedule}", "ScheduleController@destroy")->name("schedules.remove");
        Route::match(["post", "update"], "schedules", "ScheduleController@store")->name("schedules.store");

        Route::namespace("\App\Http\Controllers\Advisor")
            ->group(function () {
                Route::get("schedules", "UserController@schedules")->name("schedules.index");

                Route::get("/", "DashboardController@index")->name("dashboard.index");
                Route::get("profile", "UserController@profile")->name("profile");
                Route::post("profile/changeStatus", "UserController@changeStatus")->name("profile.changeStatus");

            });

    });
