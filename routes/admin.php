<?php

use Illuminate\Support\Facades\Route;


Route::namespace("\App\Http\Controllers\Back")->middleware(["auth","auth.admin"])->prefix("admin")->as("admin.")
    ->group(function () {

        Route::get("/", "DashboardController@index")->name("dashboard.index");

        Route::get("users/getUsers", "UserController@getUsers")->name("users.getUsers");
        Route::resource("users", "UserController")->except(["update", "store","destroy"]);
        Route::match(['PUT', "POST"], "users", "UserController@store")->name("users.store");
        Route::get("users/{user}/destroy", "UserController@destroy")->name("users.destroy");
        Route::get("users/{user}/archive", "UserController@archive")->name("users.archive");
        Route::get("users/{user}/recycle", "UserController@recycle")->name("users.recycle");
        Route::get("users/archived/list", "UserController@archivedList")->name("users.archive.index");
        Route::get("users/schedule/{user}", "UserController@scheduleForm")->name("users.schedule.form");

        Route::get("search/users", "SearchController@searchForUser")->name("search.users");

        Route::get("credits", "CreditController@index")->name("credits.index");

        Route::get("credits/show/{credit}", "CreditController@show")->name("credits.show");
        Route::post("credits/addCharge/{credit}", "CreditController@addCharge")->name("credits.addCharge");
        Route::get("credits/createUniqueUrl/{credit}", "CreditController@createUniqueUrl")->name("credits.createUniqueUrl");

        Route::get("creditGroups", "CreditGroupController@index")->name("creditGroups.index");
        Route::get("creditGroups/create", "CreditGroupController@create")->name("creditGroups.create");
        Route::post("creditGroups/{creditGroup}/createPins", "CreditGroupController@storePins")->name("creditGroups.storePins");
        Route::get("creditGroups/{creditGroup}/getCredits", "CreditGroupController@getGreditsOfGroup")->name("creditGroups.getCreditsOfGroup");
        Route::get("creditGroups/{creditGroup}/destroy", "CreditGroupController@destroy")->name("creditGroups.destroy");

        Route::get("creditGroups/{creditGroup}", "CreditGroupController@show")->name("creditGroups.show");
        Route::post("creditGroups", "CreditGroupController@store")->name("creditGroups.store");
        Route::get("creditGroups/{creditGroup}/download", "CreditGroupController@download")->name("creditGroups.download");


        Route::resource("queues", "QueueController")->except(["update", "store","destroy"]);
        Route::match(['PUT', "POST"], "queues", "QueueController@store")->name("queues.store");
        Route::get("queues/{queue}/destroy", "QueueController@destroy")->name("queues.destroy");
        Route::post("queues/addUser", "QueueController@addUser")->name("queues.add");
        Route::post("queues/changeStatus", "QueueController@changeStatus")->name("queues.changeStatus");
        Route::delete("queues/users/removeFromQueue", "QueueController@removeUserFromQueue")->name("queues.removeUserFromQueue");

        Route::get("calls","CallController@index")->name("calls.index");
        Route::get("calls/getCalls","CallController@getCalls")->name("calls.getCalls");
        Route::get("calls/{call}","CallController@show")->name("calls.show");

        Route::get("reports/calls", "ReportController@calls");

        Route::get("changePassword", "PasswordController@form")->name("changePasswordForm");
        Route::post("changePassword", "PasswordController@update")->name("changePassword");

        Route::get("search", "SearchController@search")->name("search");

        Route::get("options", "OptionsController@index")->name("options.index");
        Route::post("options", "OptionsController@store")->name("options.store");

    });
