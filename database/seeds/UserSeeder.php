<?php

use App\Classes\FakerProviders\PersianFaker;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users =
            [
                [
                    'name' => 'کاربر ادمین',
                    'email' => 'admin@demo.com',
                    'password' => bcrypt("123456"),
                    'simotel_number' => "100",
                    'mobile' => "09370000000",
                    'comment' => "",
                    'address' => "تهران، آدرس مربوط به محل کاربر ادمین",
                    'level' => User::LEVEL_ADMIN,
                    'entrance_fee' => 1000,
                    'credit_ratio' => 1000,
                ],
                [
                    'name' => 'کاربر مشاور',
                    'email' => 'advisor@demo.com',
                    'password' => bcrypt("123456"),
                    'simotel_number' => "102",
                    'mobile' => "09151515115",
                    'entrance_fee' => 1000,
                    'credit_ratio' => 1000,
                    'comment' => "",
                    'address' => "کاشمر، خیابان امام",
                    'level' => User::LEVEL_ADVISOR,
                ],
            ];

        User::insert($users);


        $faker = \Faker\Factory::create("fa_IR");
        $faker->addProvider(new PersianFaker($faker));
        $users = [];
        for ($i = 0; $i < 20; $i++) {
            $users[] = [

                'name' => $faker->name(),
                'email' => $faker->email(),
                'password' => bcrypt("123456"),
                'simotel_number' => 103 + $i,
                'mobile' => "09370331680",
                'comment' => $faker->text(50),
                'address' => $faker->text(100),
                'level' => User::LEVEL_ADVISOR,
                'entrance_fee' => 1000,
                'credit_ratio' => 1000,
            ];
        }

        User::insert($users);

    }
}
