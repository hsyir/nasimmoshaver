<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Schedule::insert([
            "user_id"=>1,
            "start_time" => "08:00",
            "end_time" => "10:00",
            "day" => "1",
        ]);
    }
}
